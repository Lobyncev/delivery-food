///////////////////
// Исходные данные
//////////////////

const cartButton = document.querySelector("#cart-button");
const modal = document.querySelector(".modal");
const close = document.querySelector(".close");

cartButton.addEventListener("click", toggleModal);
close.addEventListener("click", toggleModal);

function toggleModal() {
  modal.classList.toggle("is-open");
}



/////////////
//day 1
/////////////


/////////////
// Константы
/////////////

const buttonAuth = document.querySelector('.button-auth');
const modalAuth = document.querySelector('.modal-auth');
const closeAuth = document.querySelector('.close-auth');
const logInForm = document.querySelector('#logInForm');
const loginInput = document.querySelector('#login');
const userName = document.querySelector('.user-name');
const buttonOut = document.querySelector('.button-out');


/////////////
// Перменные
/////////////

let login = localStorage.getItem('testStorage');


//////////////////
// Введён ли логин
//////////////////

function checkAuth() {
  if (login) {
    authorized();
  } else {
    notAuthorized();
  }
}

checkAuth();


///////////////////////////////////////////
// Показать и настроить модалку авторизации
///////////////////////////////////////////

function toggleModalAuth() {
  modalAuth.classList.toggle('is-open');
}

buttonAuth.addEventListener('click', toggleModalAuth);
closeAuth.addEventListener('click', toggleModalAuth);


///////////////////////////////////////////////
// Логика авторизации (валидация, логика формы)
///////////////////////////////////////////////

function notAuthorized() {
  console.log('Не авторизован');

  function logIn(event) {
    event.preventDefault();
    login = loginInput.value;

    if (!login) {
      return loginInput.style = 'border-color: red;'
    }

    localStorage.setItem('testStorage', login);

    toggleModalAuth();

    buttonAuth.removeEventListener('click', toggleModalAuth);
    closeAuth.removeEventListener('click', toggleModalAuth);
    logInForm.removeEventListener('submit', logIn);

    logInForm.reset();

    checkAuth();
  }

  buttonAuth.addEventListener('click', toggleModalAuth);
  closeAuth.addEventListener('click', toggleModalAuth);
  logInForm.addEventListener('submit', logIn);
}


////////////////////////////////////////////////////////////
// Логика авторизованного пользователя (вывод имени, logOut)
////////////////////////////////////////////////////////////

function authorized() {
  console.log('Авторизован');

  //Логика выхода (удаляем login из localStorage, убираем кнопки)
  function logOut() {
    login = null;

    localStorage.removeItem('testStorage');

    buttonAuth.style.display = '';
    userName.style.display = '';
    buttonOut.style.display = '';

    buttonOut.removeEventListener('click', logOut);

    checkAuth();
  }

  userName.textContent = login;

  buttonAuth.style.display = 'none';
  userName.style.display = 'inline';
  buttonOut.style.display = 'block';

  buttonOut.addEventListener('click', logOut);
}